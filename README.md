<h1 align="center">Hi, I'm Andrzej</h1>
<h3 align="center">A passionate Full Stack Developer from Poland</h3>

# My projects

## (WORK IN PROGRESS) K8s Jenkins - On-Prem App

- [Source Code for the K8s Jenkins app](https://gitlab.com/wiadernyandrzej96/k8s-jenkins-app/)
- Tech stack: Kubernetes (k3s), Jenkins, ArgoCD, Artifact Registry (Do﻿ckerHub), Git (GitLab), PostgreSQL, NodeJS 

## (WORK IN PROGRESS) AWS Serverless - Serverless Cloud App

- [Source Code for the AWS Serverless Cloud app](https://gitlab.com/wiadernyandrzej96/aws-serverless)
- Tech stack: GoLang, AWS Serverless Framework, AWS Lambda, API Gateway, DynamoDB, Cloudformation, S3
